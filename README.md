# INA226 USB AMPEREMETER

The purpose of this page is to explain step by step the realization of an USB amperemeter based on ARDUINO NANO.

It can measure currents from 10µA to 0.8A.

It can work only with a charge under 5V, for example to measure the current consumed by an ARDUINO board.

This amperemeter displays the current on an OLED display.

The board uses the following components :

 * an ARDUINO NANO
 * an INA226
 * a small OLED 0.96" display
 * some passive components
 * the board is powered by the USB or two 18560 LITHIUM ION batteries.

### ELECTRONICS

The schema is made using KICAD.

### ARDUINO

The code is build using ARDUINO IDE 1.8.5.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2018/11/usb-un-voltmetre-amperemetre.html