
#include <INA226.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define OLED_RESET 4
#define ina226_OHM                0.112 // ohm
#define INA226_SHUNT              112 // mohm
#define IMAX_EXPECTED             0.500
#define OFFSET                    0.04

#define DEBUG

class MyINA226 : public INA226
{
  public:
    bool begin(uint8_t address = INA226_ADDRESS);
    int16_t readRegister16(uint8_t reg);
    int8_t addr;
};

bool MyINA226::begin(uint8_t address)
{
  INA226::begin(address);
  Wire.begin();
  addr = address;
  return true;
}

int16_t MyINA226::readRegister16(uint8_t reg)
{
  int16_t value;

  Wire.beginTransmission(addr);
#if ARDUINO >= 100
  Wire.write(reg);
#else
  Wire.send(reg);
#endif
  Wire.endTransmission();

  delay(1);

  Wire.beginTransmission(addr);
  Wire.requestFrom(addr, 2);
  while (!Wire.available()) {};
#if ARDUINO >= 100
  uint8_t vha = Wire.read();
  uint8_t vla = Wire.read();
#else
  uint8_t vha = Wire.receive();
  uint8_t vla = Wire.receive();
#endif;
  Wire.endTransmission();

  value = vha << 8 | vla;

  return value;
}

MyINA226 ina226;
Adafruit_SSD1306 display(OLED_RESET);

void checkIna226Config(INA226 *ina)
{
  Serial.print(F("Mode:                  "));
  switch (ina->getMode())
  {
    case INA226_MODE_POWER_DOWN:      Serial.println(F("Power-Down")); break;
    case INA226_MODE_SHUNT_TRIG:      Serial.println(F("Shunt Voltage, Triggered")); break;
    case INA226_MODE_BUS_TRIG:        Serial.println(F("Bus Voltage, Triggered")); break;
    case INA226_MODE_SHUNT_BUS_TRIG:  Serial.println(F("Shunt and Bus, Triggered")); break;
    case INA226_MODE_ADC_OFF:         Serial.println(F("ADC Off")); break;
    case INA226_MODE_SHUNT_CONT:      Serial.println(F("Shunt Voltage, Continuous")); break;
    case INA226_MODE_BUS_CONT:        Serial.println(F("Bus Voltage, Continuous")); break;
    case INA226_MODE_SHUNT_BUS_CONT:  Serial.println(F("Shunt and Bus, Continuous")); break;
    default: Serial.println(F("unknown"));
  }

  Serial.print(F("Samples average:       "));
  switch (ina->getAverages())
  {
    case INA226_AVERAGES_1:           Serial.println(F("1 sample")); break;
    case INA226_AVERAGES_4:           Serial.println(F("4 samples")); break;
    case INA226_AVERAGES_16:          Serial.println(F("16 samples")); break;
    case INA226_AVERAGES_64:          Serial.println(F("64 samples")); break;
    case INA226_AVERAGES_128:         Serial.println(F("128 samples")); break;
    case INA226_AVERAGES_256:         Serial.println(F("256 samples")); break;
    case INA226_AVERAGES_512:         Serial.println(F("512 samples")); break;
    case INA226_AVERAGES_1024:        Serial.println(F("1024 samples")); break;
    default: Serial.println("unknown");
  }

  Serial.print(F("Bus conversion time:   "));
  switch (ina->getBusConversionTime())
  {
    case INA226_BUS_CONV_TIME_140US:  Serial.println(F("140uS")); break;
    case INA226_BUS_CONV_TIME_204US:  Serial.println(F("204uS")); break;
    case INA226_BUS_CONV_TIME_332US:  Serial.println(F("332uS")); break;
    case INA226_BUS_CONV_TIME_588US:  Serial.println(F("558uS")); break;
    case INA226_BUS_CONV_TIME_1100US: Serial.println(F("1.100ms")); break;
    case INA226_BUS_CONV_TIME_2116US: Serial.println(F("2.116ms")); break;
    case INA226_BUS_CONV_TIME_4156US: Serial.println(F("4.156ms")); break;
    case INA226_BUS_CONV_TIME_8244US: Serial.println(F("8.244ms")); break;
    default: Serial.println(F("unknown"));
  }

  Serial.print(F("Shunt conversion time: "));
  switch (ina->getShuntConversionTime())
  {
    case INA226_SHUNT_CONV_TIME_140US:  Serial.println(F("140uS")); break;
    case INA226_SHUNT_CONV_TIME_204US:  Serial.println(F("204uS")); break;
    case INA226_SHUNT_CONV_TIME_332US:  Serial.println(F("332uS")); break;
    case INA226_SHUNT_CONV_TIME_588US:  Serial.println(F("558uS")); break;
    case INA226_SHUNT_CONV_TIME_1100US: Serial.println(F("1.100ms")); break;
    case INA226_SHUNT_CONV_TIME_2116US: Serial.println(F("2.116ms")); break;
    case INA226_SHUNT_CONV_TIME_4156US: Serial.println(F("4.156ms")); break;
    case INA226_SHUNT_CONV_TIME_8244US: Serial.println(F("8.244ms")); break;
    default: Serial.println(F("unknown"));
  }

  Serial.print(F("Max possible current:  "));
  Serial.print(ina->getMaxPossibleCurrent());
  Serial.println(F(" A"));

  Serial.print(F("Max current:           "));
  Serial.print(ina->getMaxCurrent());
  Serial.println(F(" A"));

  Serial.print(F("Max shunt voltage:     "));
  Serial.print(ina->getMaxShuntVoltage());
  Serial.println(F(" V"));

  Serial.print(F("Max power:             "));
  Serial.print(ina->getMaxPower());
  Serial.println(F(" W"));
}

float readBusVoltage(void)
{
  float busVoltage = ina226.readBusVoltage();
#ifdef DEBUG
  Serial.print(F("Bus voltage: "));
  Serial.print(busVoltage, 3);
  Serial.println(F(" V"));
#endif
  return busVoltage;
}

float readShuntCurrent(void)
{
  //  float shuntCurrent = ina226.readShuntCurrent() * 1000;
  long shuntVoltage = ina226.readRegister16(INA226_REG_SHUNTVOLTAGE) * 25L / 10;
  float shuntCurrent = (float)shuntVoltage / INA226_SHUNT + OFFSET;
#ifdef DEBUG
  Serial.print(F("Shunt voltage: "));
  Serial.print(shuntVoltage);
  Serial.println(F(" µV"));
  Serial.print(F("Shunt current: "));
  Serial.print(shuntCurrent);
  Serial.println(F(" mA"));
#endif
  return shuntCurrent;
}

void setup()
{
  char *title = "USB POWER METER";
  Serial.begin(115200);
  Serial.println(title);
  ina226.begin(0x41);
  ina226.configure(INA226_AVERAGES_16, INA226_BUS_CONV_TIME_8244US, INA226_SHUNT_CONV_TIME_8244US, INA226_MODE_SHUNT_BUS_CONT);
  ina226.calibrate(ina226_OHM, IMAX_EXPECTED);
#ifdef DEBUG
  checkIna226Config(&ina226);
#endif
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  display.clearDisplay();
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(0, 0);
  display.print(title);
  display.display();
  delay(2000);
  display.clearDisplay();
}

void loop()
{
  float busVoltage = readBusVoltage();
  float shuntCurrent = readShuntCurrent();
  display.clearDisplay();
  display.setTextSize(2);
  display.setCursor(0, 0);
  display.print(busVoltage);
  display.println(F(" V"));
  display.print(shuntCurrent);
  display.println(F(" mA"));
  display.display();
  delay(500);
}

